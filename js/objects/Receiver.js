class Receiver {
    constructor(posX, posZ, model, doorID) {
        this.posX = posX;
        this.posZ = posZ;
        this.model = model;
        //this.object3D = model.scene;
        this.doorID = doorID;
        this.createReceiver();
    }

    createReceiver() {
        var geometry = new THREE.BoxGeometry(Settings.width / 3, Settings.width / 3, Settings.width / 3);
        // var material = new THREE.MeshBasicMaterial({ color: 0x00ccff });
        if (this.doorID != "") {
            receiversButtons.push(this);
        }
        else {
            receiversWin.push(this);
        }

        this.object3D = new THREE.Mesh(geometry, this.model);
        this.object3D.position.set(this.posX * Settings.width + Settings.width / 2, Settings.width / 6, this.posZ * Settings.width + Settings.width / 2);
        this.object3D.userData.haveLaser = false;
        this.object3D.userData.name = "receiver";
        this.ring = new Ring();
        this.ring.visible = false;
        this.ring.position.y = -Settings.width / 3 + 9;
        this.object3D.add(this.ring);
        objects.push(this.object3D);
        receivers.push(this);
        receiversObj.push(this.object3D);

    }

    getObject3D() {
        return this.object3D;
    }
}