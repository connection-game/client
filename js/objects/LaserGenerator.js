class LaserGenerator {
    constructor(posX, posZ, modelLaser, modelNoLaser, direction, rotationY) {
        this.posX = posX;
        this.posZ = posZ;
        this.direction = direction;
        this.modelLaser = modelLaser;
        this.modelNoLaser = modelNoLaser;
        this.rotationY = rotationY;
        this.object3D;

        this.lasers = [];
        this.createLaserGenerator();
    }

    createLaserGenerator() {
        var geometry = new THREE.BoxGeometry(Settings.width / 2, Settings.width / 2, Settings.width / 2);
        var materials = [];
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelNoLaser.map }));
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelNoLaser.map }));
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelNoLaser.map }));
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelNoLaser.map }));
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelLaser.map }));
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelNoLaser.map }));
        this.object3D = new THREE.Mesh(geometry, materials);
        this.object3D.position.set(this.posX * Settings.width + Settings.width / 2, Settings.width / 4, this.posZ * Settings.width + Settings.width / 2);
        this.object3D.rotation.y = this.rotationY;
        this.object3D.userData.name = "generator";
        objects.push(this.object3D);
        laserGenerators.push(this);
        laserGeneratorsObj.push(this.object3D);
        this.createFirstLaser();
    }

    createFirstLaser() {
        this.lasers.push(new Laser(this.object3D.position, this.direction, this, null, true));
    }

    getObject3D() {
        return this.object3D;
    }
}