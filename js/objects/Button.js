class Button {
    constructor(posX, posZ, model, doorID) {
        this.posX = posX;
        this.posZ = posZ;
        this.doorID = doorID;
        //this.model = model;
        //this.object3D = model.scene;

        this.createButton();
    }

    createButton() {
        var geometry = new THREE.BoxGeometry(Settings.width / 2.5, 2, Settings.width / 2.5);
        var material = new THREE.MeshBasicMaterial({ color: 0xcccccc });
        this.object3D = new THREE.Mesh(geometry, material);
        this.object3D.position.set(this.posX * Settings.width + Settings.width / 2, 1, this.posZ * Settings.width + Settings.width / 2);
        buttons.push(this);
    }

    getObject3D() {
        return this.object3D;
    }
}