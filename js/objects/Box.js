class Box {
    constructor(posX, posZ, model) {
        this.posX = posX;
        this.posZ = posZ;
        this.model = model;
        // this.object3D = model.scene.clone();
        this.createBox();
    }

    createBox() {
        var geometry = new THREE.BoxGeometry(Settings.width / 3, Settings.width / 3, Settings.width / 3);
        this.object3D = new THREE.Mesh(geometry, this.model);
        // this.object3D.scale.set(Settings.width / 120, Settings.width / 120, Settings.width / 120);
        this.object3D.position.set(this.posX * Settings.width + Settings.width / 2, Settings.width / 6, this.posZ * Settings.width + Settings.width / 2);
        this.object3D.userData.name = "box";
        objects.push(this.object3D);
        moveable.push(this);
    }

    getObject3D() {
        return this.object3D;
    }
}