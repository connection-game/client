class Helper {
    constructor(x, z, model, helpMessage) {
        this.posX = x;
        this.posZ = z;
        this.object3D = model.scene.clone();
        this.helpMessage = helpMessage;
        this.createHelper();
    }

    createHelper() {
        // this.object3D.scale.set(Settings.width / 120, Settings.width / 120, Settings.width / 120);
        this.object3D.position.set(this.posX * Settings.width + Settings.width / 2, 50, this.posZ * Settings.width + Settings.width / 2);
        this.object3D.userData.name = "helper";
        this.light = new THREE.SpotLight(0xffff00, 2, 200, 0.25, 1);
        this.light.target.position.set(0, 0, 0);
        this.object3D.add(this.light);
        this.object3D.add(this.light.target);
        helpers.push(this);
    }

    getObject3D() {
        return this.object3D;
    }
}