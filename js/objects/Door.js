class Door {
    constructor(posX, posZ, model, doorID) {
        this.posX = posX;
        this.posZ = posZ;
        this.model = model;
        this.doorID = doorID;
        //this.object3D = model.scene;

        this.createDoor();
    }

    createDoor() {
        var geometry = new THREE.BoxGeometry(Settings.width / 1.5, Settings.width / 1.5, Settings.width / 1.5);
        this.object3D = new THREE.Mesh(geometry, this.model);
        this.object3D.position.set(this.posX * Settings.width + Settings.width / 2, Settings.width / 3, this.posZ * Settings.width + Settings.width / 2);
        this.object3D.userData.name = "door";
        this.object3D.userData.isOpen = false;
        objects.push(this.object3D);
        doorsObj.push(this.object3D);
        doors.push(this);
    }

    getObject3D() {
        return this.object3D;
    }
}