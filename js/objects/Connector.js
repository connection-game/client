class Connector {
    constructor(posX, posZ, modelSide, modelTop, modelEmptySide, moveable) {
        this.posX = posX;
        this.posZ = posZ;
        this.modelSide = modelSide;
        this.modelTop = modelTop;
        this.modelEmptySide = modelEmptySide;
        this.moveable = moveable;
        // this.object3D = model.scene;
        this.createConnector();
    }

    createConnector() {
        var geometry = new THREE.BoxGeometry(Settings.width / 4, Settings.width / 2, Settings.width / 4);
        var materials = [];
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelSide.map }));
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelSide.map }));
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelTop.map }));
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelTop.map }));
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelSide.map }));
        materials.push(new THREE.MeshBasicMaterial({ map: this.modelSide.map }));
        this.object3D = new THREE.Mesh(geometry, materials);
        this.object3D.position.set(this.posX * Settings.width + Settings.width / 2, Settings.width / 4, this.posZ * Settings.width + Settings.width / 2);
        this.object3D.userData.name = "connector";
        this.object3D.userData.haveLaser = false;
        this.object3D.userData.laser = null;
        this.ring = new Ring();
        this.ring.visible = false;
        this.ring.position.y = -Settings.width / 2 + 9;
        this.object3D.add(this.ring);
        objects.push(this.object3D);
        connectors.push(this);
        if (this.moveable) {
            moveable.push(this);
        }
    }

    changeRing(visible) {
        this.ring.visible = visible;
    }

    changeMaterial() {
        var materials = [];
        if (this.object3D.userData.haveLaser) {
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelSide.map }));
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelSide.map }));
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelTop.map }));
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelTop.map }));
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelSide.map }));
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelSide.map }));
        } else {
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelEmptySide.map }));
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelEmptySide.map }));
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelTop.map }));
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelTop.map }));
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelEmptySide.map }));
            materials.push(new THREE.MeshBasicMaterial({ map: this.modelEmptySide.map }));
        }
        this.object3D.material = materials;
    }

    getObject3D() {
        return this.object3D;
    }
}