class Net {
    constructor() {
        this.beginCal = function () { };
        if (location.protocol == "https:") {
            this.webSocket = new WebSocket("wss://" + window.location.hostname + ":" + location.port + "/");
        }
        else {
            this.webSocket = new WebSocket("ws://" + window.location.hostname + ":" + location.port + "/");
        }
        this.webSocket.onmessage = (event) => {
            var obj = JSON.parse(event.data);
            switch (obj.type) {
                case "roomInfo":
                    playerId = obj.data.numOfPlayers - 1;
                    if (playerId == 1) {
                        levelsShow();
                        document.getElementById("waitingScreen").style.opacity = "0";
                        // document.getElementById("waitingMessage").innerText = "Ładowanie poziomu";
                        createGame();
                    }
                    console.log(obj.type, playerId, obj.to);
                    break;
                case "playerConnected":
                    console.log(obj.type);
                    levelsShow();
                    document.getElementById("waitingScreen").style.opacity = "0";
                    // document.getElementById("waitingMessage").innerText = "Ładowanie poziomu";
                    createGame();
                    break;
                case "playerDisconnected":
                    console.log(obj.type);
                    // alert("Drugi gracz rozłączył się!");
                    window.location.reload();
                    break;
                case "levelLoad":
                    loadLevelFun(obj.data.levelName);
                    levelsHide();
                    document.getElementById("waitingMessage").innerText = "Ładowanie poziomu";
                    document.getElementById("waitingScreen").style.opacity = "1";
                    break;
                case "levelLoaded":
                    otherLoaded = true;
                    if (!levelLoading) {
                        this.beginCal();
                    }
                    break;
                case "playerMovePos":
                    players[Math.abs(playerId - 1)].setPos(obj.data.rotation, obj.data.posX, obj.data.posZ, obj.data.posXgrid, obj.data.posZgrid);
                    break;
                case "playerHoldItem":
                    console.log(players[Math.abs(playerId - 1)].holding, players[Math.abs(playerId - 1)].holdingItem);
                    players[Math.abs(playerId - 1)].takeItem(obj.data.posX, obj.data.posZ);
                    break;
                case "createLaser":
                    players[Math.abs(playerId - 1)].createLaser(new THREE.Vector3(obj.data.colObjPos.x, obj.data.colObjPos.y, obj.data.colObjPos.z), obj.data.startConX, obj.data.startConZ);
                    break;
                case "clearLaser":
                    laserGenerators.forEach((laserGenerator) => {
                        if (laserGenerator.posX == obj.data.genPosX && laserGenerator.posZ == obj.data.genPosZ) {
                            laserGenerator.lasers[obj.data.laserIndex].clearLasers(obj.data.laserIndex, laserGenerator);
                        }
                    });
                    break;
            }
        }
    }

    send(type, data) {
        var message = {
            to: "broadcast",
            type: type,
            data: data
        }
        this.webSocket.send(JSON.stringify(message));
    }

    setCallback(fun) {
        this.beginCal = fun;
    }
}