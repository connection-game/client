class Ring extends THREE.Mesh {
    constructor() {
        super();
        this.geometry = new THREE.RingGeometry(9, 12, 6);
        this.material = new THREE.MeshBasicMaterial({ color: 0x92ff92, side: THREE.DoubleSide });
        this.rotation.x = Math.PI / 2;
    }
}