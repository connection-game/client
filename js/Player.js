class Player {
    constructor(posX, posZ, model) {
        this.posX = posX;
        this.posZ = posZ;
        this.walking = false;
        this.holding = false;
        this.holdingItem = null;
        this.directionVect = new THREE.Vector3(0, 0, -1);
        this.model = model;
        this.object3D = model.scene;
        this.keys = (this.keys || []);
        this.raycaster = new THREE.Raycaster();
        this.mouseRaycaster = new THREE.Raycaster();
        this.mouseVector = new THREE.Vector2();
        this.raycaster.far = 13.5 + Settings.width / 2;
        this.raycaster.near = 13.5;
        moveable.push(this);
        this.createPlayer();
    }

    createPlayer() {
        this.object3D.scale.set(Settings.width / 300, Settings.width / 300, Settings.width / 300);
        this.object3D.position.set(this.posX * Settings.width + Settings.width / 2, 0, this.posZ * Settings.width + Settings.width / 2);
        this.object3D.rotation.y = Math.PI;
        this.object3D.userData.name = "player";
        this.playAnimation(this.model);
    }

    playAnimation(animObject) {
        this.clock = new THREE.Clock();
        this.mixer = new THREE.AnimationMixer(animObject.scene);
        var clips = animObject.animations;
        var clip = THREE.AnimationClip.findByName(clips, 'Take 001');
        var action = this.mixer.clipAction(clip);
        action.play();
    }

    addControls() {
        $(document).off("keydown");
        $(document).off("keyup");
        $(document).off("mousedown");
        $(document).off("mousemove");
        $(document).keydown((e) => {
            this.keys[e.keyCode] = true;
            if (this.keys && this.keys[32] && !this.walking && !levelSelecting) {
                this.takeItem(this.posX, this.posZ);
                console.log("playerHoldItem");
                net.send("playerHoldItem", { posX: this.posX, posZ: this.posZ });
            }
            if (this.keys && this.keys[67] && !this.walking && !levelSelecting) {
                connectors.forEach((obj) => {
                    if (obj.posX == this.posX && obj.posZ == this.posZ) {
                        laserGenerators.forEach((laserGenerator) => {
                            laserGenerator.lasers.forEach((laser, index) => {
                                if (laser.connector == obj.object3D) {
                                    net.send("clearLaser", { laserIndex: index, genPosX: laserGenerator.posX, genPosZ: laserGenerator.posZ });
                                    laser.clearLasers(index, laserGenerator);
                                }
                            });
                        });
                    }
                });
            }
        })
        $(document).keyup((e) => {
            this.keys[e.keyCode] = false;
        });
        $(document).mousedown((e) => {
            if (e.button == 0) { //LPM
                var canCreateLaser = false;
                var startConnector = null;
                connectors.concat(receivers).forEach((object) => {
                    if (object.object3D.userData.name == "connector" && object.posX == this.posX && object.posZ == this.posZ && object.object3D.userData.haveLaser && !object.object3D.userData.sendLaser) {
                        startConnector = object;
                        canCreateLaser = true;
                    }
                });
                if (canCreateLaser) {
                    this.mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
                    this.mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
                    this.mouseRaycaster.setFromCamera(this.mouseVector, camera);

                    var intersects = this.mouseRaycaster.intersectObjects(objects, true);
                    if (intersects.length > 0) {
                        this.findUserData(intersects[0].object);
                        if (!this.colObject.userData.haveLaser && (this.colObject.userData.name == "connector" || this.colObject.userData.name == "receiver")) {
                            this.createLaser(this.colObject.position.clone(), startConnector.posX, startConnector.posZ);
                            net.send("createLaser", { colObjPos: this.colObject.position.clone(), startConX: startConnector.posX, startConZ: startConnector.posZ });
                        }
                    }
                }
            }
        });
        $(document).mousemove((e) => {
            connectors.concat(receivers).forEach(element => {
                element.ring.visible = false;
            });
            connectors.concat(receivers).forEach(element => {
                if (element.posX == this.posX && element.posZ == this.posZ && element.object3D.userData.haveLaser) {
                    this.mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
                    this.mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
                    this.mouseRaycaster.setFromCamera(this.mouseVector, camera);

                    var intersects = this.mouseRaycaster.intersectObjects(objects, true);
                    if (intersects.length > 0) {
                        this.findUserData(intersects[0].object);
                        connectors.concat(receivers).forEach(elementCol => {
                            if (elementCol.object3D == this.colObject && (elementCol.posX != this.posX || elementCol.posZ != this.posZ)) {
                                elementCol.ring.visible = true;
                            }
                        });
                    }
                }
            });
        });
    }

    setPos(rotation, x, z, posX, posZ) {
        this.object3D.rotation.y = rotation;
        this.object3D.position.x = x;
        this.object3D.position.z = z;
        this.posX = posX;
        this.posZ = posZ;
        if (this.holding) {
            this.holdingItem.object3D.rotation.y = this.object3D.rotation.y;
            this.holdingItem.object3D.position.x = this.object3D.position.x;
            this.holdingItem.object3D.position.z = this.object3D.position.z;
            this.holdingItem.posX = this.posX;
            this.holdingItem.posZ = this.posZ;
        }
    }

    move(rotation, rayVect, newPosX, newPosZ, thisPlayer) {
        connectors.forEach(element => {
            element.ring.visible = false;
        });
        this.object3D.rotation.y = rotation;
        net.send("playerMovePos", { rotation: this.object3D.rotation.y, posX: this.object3D.position.x, posZ: this.object3D.position.z, posXgrid: this.posX, posZgrid: this.posZ });
        this.raycaster.set(this.object3D.position, rayVect);
        if (!this.checkCollisions(newPosX, newPosZ)) {
            this.posX = newPosX;
            this.posZ = newPosZ;
            this.targetVect = new THREE.Vector3(this.posX * Settings.width + Settings.width / 2, 0, this.posZ * Settings.width + Settings.width / 2);
            this.walking = true;
        }
    }

    walk() {
        net.send("playerMovePos", { rotation: this.object3D.rotation.y, posX: this.object3D.position.x, posZ: this.object3D.position.z, posXgrid: this.posX, posZgrid: this.posZ });
        this.object3D.translateOnAxis(this.directionVect, Settings.player.speed);
        if (this.object3D.position.clone().distanceTo(this.targetVect) < 1) {
            this.object3D.position.x = this.targetVect.x;
            this.object3D.position.z = this.targetVect.z;
            net.send("playerMovePos", { rotation: this.object3D.rotation.y, posX: this.object3D.position.x, posZ: this.object3D.position.z, posXgrid: this.posX, posZgrid: this.posZ });
            this.walking = false;
        }
        if (this.holding) {
            this.holdingItem.object3D.position.x = this.object3D.position.x;
            this.holdingItem.object3D.position.z = this.object3D.position.z;
            this.holdingItem.posX = this.posX;
            this.holdingItem.posZ = this.posZ;
            if (this.holdingItem.object3D.userData.name == "connector") {
                laserGenerators.forEach((laserGenerator) => {
                    laserGenerator.lasers.forEach((laser, index) => {
                        if (laser.connector == this.holdingItem.object3D) {
                            laser.clearLasers(index, laserGenerator);
                        }
                    });
                });
            }
        }
    }

    checkCollisions(newPosX, newPosZ) {
        this.intersects = this.raycaster.intersectObjects(objects.concat(walls.concat(glasses)), true);
        if (this.intersects.length != 0) {
            this.findUserData(this.intersects[0].object);
            if (this.colObject.userData.name != "wall" && this.colObject.userData.name != "glass" && this.colObject.userData.name != "door" && this.colObject.userData.name != "receiver" && this.colObject.userData.name != "generator") {
                if (newPosX == players[Math.abs(playerId - 1)].posX && newPosZ == players[Math.abs(playerId - 1)].posZ) {
                    return true;
                }
                return false;
            }
            for (var i = 0; i < doors.length; i++) {
                if (doors[i].object3D == this.colObject && doors[i].object3D.userData.isOpen) {
                    return false;
                }
            }
            return true;
        }
        if (newPosX == players[Math.abs(playerId - 1)].posX && newPosZ == players[Math.abs(playerId - 1)].posZ) {
            return true;
        }
        return false;
    }

    findUserData(data) {
        if (data != null) {
            if (Object.keys(data.userData).length != 0)
                this.colObject = data;
            else
                this.findUserData(data.parent);
        }
    }

    getObject3D() {
        return this.object3D;
    }

    takeItem(posX, posZ) {
        if (this.holding) {
            this.holding = false;
            this.holdingItem = null;
        } else {
            moveable.forEach(obj => {
                if (obj.posX == posX && obj.posZ == posZ && obj.object3D.userData.name != "player") {
                    this.holding = true;
                    this.holdingItem = obj;
                    if (this.holdingItem.object3D.userData.name == "connector") {
                        laserGenerators.forEach((laserGenerator) => {
                            laserGenerator.lasers.forEach((laser, index) => {
                                if (laser.connector == this.holdingItem.object3D) {
                                    laser.clearLasers(index, laserGenerator);
                                }
                            });
                        })
                    }
                }
            });
        }
    }

    createLaser(colObjPos, startConX, startConZ) {
        laserGenerators.concat(connectors).forEach(obj => {
            if (obj.posX == startConX && obj.posZ == startConZ) {
                var v3 = new THREE.Vector3(this.object3D.position.x, Settings.width / 4, this.object3D.position.z);
                var laser = new Laser(v3, colObjPos, obj.object3D.userData.laser.generator, obj);
                obj.object3D.userData.laser.generator.lasers.push(laser);
                obj.object3D.userData.sendLaser = true;
            }
        });
    }
}