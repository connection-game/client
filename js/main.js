var scene, camera, level;
var laserGenerators = [], moveable = [], receivers = [], receiversWin = [], receiversButtons = [], doors = [], players = [], connectors = [], buttons = [], helpers = [];
var objects = [], doorsObj = [], receiversObj = [], laserGeneratorsObj = [], walls = [], glasses = [], fogs = [];
var wallTemplate, floorTemplate, glassTemplate, fogTemplate;
var winBoard = false;
var playerId = null;
var net;
var renderExist = false;
var levelLoading = false, otherLoaded = false, loadLevelFun = null;
var levelSelecting = false;

$(document).ready(function () {
    var oldInfoFunction = console.warn;
    console.warn = function () { };
    net = new Net();
});

function levelsShow() {
    while (document.getElementById("levelsList").firstChild) {
        document.getElementById("levelsList").removeChild(document.getElementById("levelsList").firstChild);
    }
    fetch('../levels/')
        .then((response) => response.json())
        .then((levelArray) => {
            levelArray.forEach(level => {
                var levelRow = document.createElement("div");
                levelRow.classList.add("levelsRow");
                levelRow.innerText = level;
                levelRow.addEventListener("click", (e) => {
                    levelsHide();
                    document.getElementById("waitingScreen").style.opacity = "1";
                    document.getElementById("waitingMessage").innerText = "Ładowanie poziomu";
                    net.send("levelLoad", { levelName: e.currentTarget.innerText });
                    loadLevelFun(e.currentTarget.innerText);
                })
                document.getElementById("levelsList").appendChild(levelRow);
            });
            document.getElementById("selectLevel").style.visibility = "visible";
            document.getElementById("selectLevel").style.opacity = "1";
        })
    levelSelecting = true;
}

function levelsHide() {
    document.getElementById("selectLevel").style.opacity = "0";
    setTimeout(() => {
        document.getElementById("selectLevel").style.visibility = "hidden";
    }, 350)
    levelSelecting = false;
}

function createGame() {
    loadLevelFun = loadLevel;
    var stats = new Stats();
    stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
    document.body.appendChild(stats.dom);

    // loadLevel('doorTest');

    //RENDERER
    var renderer = new THREE.WebGLRenderer({ alpha: true, antialias: false });
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.setSize($("#root").width(), $("#root").height());
    $("#root").append(renderer.domElement);

    window.onwheel = (e) => {
        if (!levelSelecting) {
            Settings.camera.y += e.deltaY / 10;
            if (Settings.camera.y < 40) {
                Settings.camera.y = 40;
            }
            if (Settings.camera.y > 500) {
                Settings.camera.y = 500;
            }
        }
    }

    $(window).keydown((e) => {
        if (e.keyCode == 27) {
            if (!levelSelecting && !levelLoading && otherLoaded) {
                levelsShow();
            } else {
                levelsHide();
            }
        }
    })

    //ORBIT CONTOLS
    // var orbitControl = new THREE.OrbitControls(camera, renderer.domElement);
    // orbitControl.addEventListener('change', function () {
    //     renderer.render(scene, camera)
    // });

    function createRender() {
        if (!renderExist) {
            render();
            renderExist = true;
            document.getElementById("end").style.display = "flex";
        }
        document.getElementById("root").style.opacity = "1";
        document.getElementById("waitingScreen").style.opacity = "0";
    }

    function loadLevel(levelName) {
        levelLoading = true, otherLoaded = false;

        scene = new THREE.Scene();
        // scene.fog = new THREE.Fog("#cccccc", 100, 2000);

        level = null;
        laserGenerators = [], moveable = [], receivers = [], receiversWin = [], receiversButtons = [], doors = [], players = [], connectors = [], buttons = [], helpers = [];
        objects = [], doorsObj = [], receiversObj = [], laserGeneratorsObj = [], walls = [], glasses = [], fogs = [];
        winBoard = false;

        camera = new THREE.PerspectiveCamera(
            45, // fov
            $(window).width() / $(window).height(), // proporcje widokui
            0.1, // minimalna renderowana odległość
            10000 // maxymalna renderowana odległość od kamery 
        );

        //RESIZE WINDOW
        $(window).resize(function () {
            camera.aspect = $(window).width() / $(window).height()
            camera.updateProjectionMatrix();
            renderer.setSize($(window).width(), $(window).height())
        });

        //LIGHT
        var light = new THREE.AmbientLight(0xffffff, 1);
        scene.add(light);

        fetch('../levels/' + levelName)
            .then((response) => response.json())
            .then((myJson) => {
                level = myJson;
                var manager = new ContentManager();
                manager.addModel("player1", "gfx/Drone/gltf/scene.gltf");
                manager.addModel("player2", "gfx/Drone/gltf/scene.gltf");
                // manager.addModel("box", "gfx/crate_box/gltf/scene.gltf");
                manager.addModel("helper", "gfx/help/gltf/scene.gltf");
                manager.addPhongMaterial("wall", "gfx/textures/blue.png");
                manager.addPhongMaterial("glass", "gfx/textures/glass.png");
                manager.addPhongMaterial("fog", "gfx/textures/fog.png");
                manager.addPhongMaterial("box", "gfx/textures/box1.png");
                manager.addPhongMaterial("generatorLaser", "gfx/textures/generatorLaser.png");
                manager.addPhongMaterial("generatorNoLaser", "gfx/textures/generatorNoLaser.png");
                manager.addPhongMaterial("connectorMoveableSide", "gfx/textures/connectorMoveableSide.png");
                manager.addPhongMaterial("connectorNotMoveableSide", "gfx/textures/connectorNotMoveableSide.png");
                manager.addPhongMaterial("connectorEmptyMoveableSide", "gfx/textures/connectorEmptyMoveableSide.png");
                manager.addPhongMaterial("connectorEmptyNotMoveableSide", "gfx/textures/connectorEmptyNotMoveableSide.png");
                manager.addPhongMaterial("connectorMoveableTop", "gfx/textures/connectorMoveableTop.png");
                manager.addPhongMaterial("connectorNotMoveableTop", "gfx/textures/connectorNotMoveableTop.png");
                manager.addPhongMaterial("receiver", "gfx/textures/receiver.png");
                manager.addPhongMaterial("receiverDoor", "gfx/textures/receiverDoor.png");
                manager.addPhongMaterial("door", "gfx/textures/door.png");
                manager.addPhongMaterial("floor", "gfx/textures/metalFloor.png");
                manager.loaded()
                    .then(() => {
                        wallTemplate = new Wall(manager.data["wall"]);
                        glassTemplate = new Glass(manager.data["glass"]);
                        fogTemplate = new Fog(manager.data["fog"]);
                        floorTemplate = new Floor(manager.data["floor"]);

                        //LEVEL
                        for (var z = 0; z < level.data.length; z++) {
                            for (var x = 0; x < level.data[z].length; x++) {
                                if (level.data[z][x] != null) {
                                    if (level.data[z][x].wall != null) {
                                        for (var i = 0; i < 4; i++) {
                                            switch (i) {
                                                case 0:
                                                    if (level.data[z][x].wall.left == "wall") {
                                                        wallGenerate(x * Settings.width, z * Settings.width + Settings.width / 2, Math.PI / 2);
                                                    } else if (level.data[z][x].wall.left == "glass") {
                                                        glassGenerate(x * Settings.width, z * Settings.width + Settings.width / 2, Math.PI / 2);
                                                    } else if (level.data[z][x].wall.left == "fog") {
                                                        fogGenerate(x * Settings.width, z * Settings.width + Settings.width / 2, Math.PI / 2);
                                                    }
                                                    break;
                                                case 1:
                                                    if (level.data[z][x].wall.right == "wall") {
                                                        wallGenerate((x + 1) * Settings.width, z * Settings.width + Settings.width / 2, Math.PI / 2);
                                                    } else if (level.data[z][x].wall.right == "glass") {
                                                        glassGenerate((x + 1) * Settings.width, z * Settings.width + Settings.width / 2, Math.PI / 2);
                                                    } else if (level.data[z][x].wall.right == "fog") {
                                                        fogGenerate((x + 1) * Settings.width, z * Settings.width + Settings.width / 2, Math.PI / 2);
                                                    }
                                                    break;
                                                case 2:
                                                    if (level.data[z][x].wall.top == "wall") {
                                                        wallGenerate(x * Settings.width + Settings.width / 2, z * Settings.width, 0);
                                                    } else if (level.data[z][x].wall.top == "glass") {
                                                        glassGenerate(x * Settings.width + Settings.width / 2, z * Settings.width, 0);
                                                    } else if (level.data[z][x].wall.top == "fog") {
                                                        fogGenerate(x * Settings.width + Settings.width / 2, z * Settings.width, 0);
                                                    }
                                                    break;
                                                case 3:
                                                    if (level.data[z][x].wall.bottom == "wall") {
                                                        wallGenerate(x * Settings.width + Settings.width / 2, (z + 1) * Settings.width, 0);
                                                    } else if (level.data[z][x].wall.bottom == "glass") {
                                                        glassGenerate(x * Settings.width + Settings.width / 2, (z + 1) * Settings.width, 0);
                                                    } else if (level.data[z][x].wall.bottom == "fog") {
                                                        fogGenerate(x * Settings.width + Settings.width / 2, (z + 1) * Settings.width, 0);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    if (level.data[z][x].block != null) {
                                        switch (level.data[z][x].block.block) {
                                            case "generator":
                                                scene.add(new LaserGenerator(x, z, manager.data["generatorLaser"], manager.data["generatorNoLaser"], dirToVector[level.data[z][x].block.direction], dirToRotation[level.data[z][x].block.direction]).getObject3D());
                                                break;
                                            case "connector":
                                                if (level.data[z][x].block.moveable) {
                                                    scene.add(new Connector(x, z, manager.data["connectorMoveableSide"], manager.data["connectorMoveableTop"], manager.data["connectorEmptyMoveableSide"], level.data[z][x].block.moveable).getObject3D());
                                                } else {
                                                    scene.add(new Connector(x, z, manager.data["connectorNotMoveableSide"], manager.data["connectorNotMoveableTop"], manager.data["connectorEmptyNotMoveableSide"], level.data[z][x].block.moveable).getObject3D());
                                                }
                                                break;
                                            case "receiver":
                                                if (level.data[z][x].block.objectId == "") {
                                                    scene.add(new Receiver(x, z, manager.data["receiver"], level.data[z][x].block.objectId).getObject3D());
                                                } else {
                                                    scene.add(new Receiver(x, z, manager.data["receiverDoor"], level.data[z][x].block.objectId).getObject3D());
                                                }
                                                break;
                                            case "box":
                                                scene.add(new Box(x, z, manager.data["box"]).getObject3D());
                                                break;
                                            case "helper":
                                                scene.add(new Helper(x, z, manager.data["helper"], level.data[z][x].block.text).getObject3D());
                                                break;
                                            case "button":
                                                scene.add(new Button(x, z, "", level.data[z][x].block.objectId).getObject3D());
                                                break;
                                            case "door":
                                                scene.add(new Door(x, z, manager.data["door"], level.data[z][x].block.id).getObject3D());
                                                break;
                                            case "player1":
                                                players[0] = new Player(x, z, manager.data["player1"]);
                                                scene.add(players[0].getObject3D());
                                                break;
                                            case "player2":
                                                players[1] = new Player(x, z, manager.data["player2"]);
                                                scene.add(players[1].getObject3D());
                                                break;
                                        }
                                    }
                                }
                                if (level.data[z][x] != null) {
                                    //FLOOR
                                    var floor = floorTemplate.clone();
                                    floor.position.set(x * Settings.width + Settings.width / 2, 0, z * Settings.width + Settings.width / 2);
                                    floor.rotation.y = Math.PI / 2;
                                    floor.material = floorTemplate.material;
                                    scene.add(floor);

                                    //SIATKA
                                    var piece = new GridPiece(x, z);
                                    piece.object3D.position.y = 1;
                                    scene.add(piece.object3D);
                                }
                            }
                        }

                        //ADD PROPERTIES TO ARRAYS
                        players[playerId].addControls();
                        moveable.forEach(obj => {
                            obj.actualButton = null;
                        });

                        net.setCallback(() => createRender());
                        net.send("levelLoaded", {});
                        levelLoading = false;
                        if (otherLoaded) {
                            createRender();
                        }
                    })
                    .catch((err) => {
                        console.log("error", err);
                        levelsShow();
                    });
            });

    }

    function render() {
        stats.begin();

        if (!levelLoading) {
            players.forEach((player) => {
                var delta = player.clock.getDelta();
                player.mixer.update(delta);
            });

            //CONNECTOR CHANGE MATERIAL
            connectors.forEach((connector) => {
                connector.changeMaterial();
            });

            //PLAYER MOVEMENT
            if (!players[playerId].walking && !levelSelecting) {
                if (players[playerId].keys[87]) {
                    players[playerId].move(0, new THREE.Vector3(0, 0, -1), players[playerId].posX, players[playerId].posZ - 1, true);
                } else if (players[playerId].keys[83]) {
                    players[playerId].move(Math.PI, new THREE.Vector3(0, 0, 1), players[playerId].posX, players[playerId].posZ + 1, true);
                } else if (players[playerId].keys[65]) {
                    players[playerId].move(Math.PI / 2, new THREE.Vector3(-1, 0, 0), players[playerId].posX - 1, players[playerId].posZ, true);
                } else if (players[playerId].keys[68]) {
                    players[playerId].move(Math.PI * 1.5, new THREE.Vector3(1, 0, 0), players[playerId].posX + 1, players[playerId].posZ, true);
                }
            }
            if (players[playerId].holding) {
                players[playerId].holdingItem.object3D.rotation.y = players[playerId].object3D.rotation.y;
            }
            if (players[playerId].walking) {
                players[playerId].walk();
            }

            //BUTTONS & DOORS
            buttons.forEach((button) => {
                moveable.forEach((obj) => {
                    if (button.posX == obj.posX && button.posZ == obj.posZ) {
                        obj.actualButton = button.doorID;
                    }
                });
            });
            doors.forEach((door) => {
                door.object3D.userData.isOpen = false;
                moveable.forEach((obj) => {
                    if (obj.actualButton == door.doorID) {
                        door.object3D.userData.isOpen = true;
                    }
                });
                receiversButtons.forEach((receiver) => {
                    if (receiver.object3D.userData.haveLaser && receiver.doorID == door.doorID) {
                        door.object3D.userData.isOpen = true;
                    }
                });
                if (door.object3D.position.y > -Settings.width / 3 + 2 && door.object3D.userData.isOpen) {
                    door.object3D.position.y -= 0.4;
                }
                if (door.object3D.position.y < Settings.width / 3 && !door.object3D.userData.isOpen) {
                    door.object3D.position.y += 0.4;
                }
            });
            moveable.forEach(obj => {
                obj.actualButton = null;
            });

            //HELPERS
            var onHelp = false;
            helpers.forEach(helper => {
                if (helper.posX == players[playerId].posX && helper.posZ == players[playerId].posZ) {
                    document.getElementById("solutionMessage").innerText = helper.helpMessage;
                    document.getElementById("solution").style.opacity = "1";
                    onHelp = true;
                }
            });
            if (!onHelp) {
                document.getElementById("solution").style.opacity = "0";
            }

            //CAMERA PLAYER
            camera.position.x = players[playerId].getObject3D().position.x + Settings.camera.x;
            camera.position.y = players[playerId].getObject3D().position.y + Settings.camera.y;
            camera.position.z = players[playerId].getObject3D().position.z + Settings.camera.z;
            camera.lookAt(players[playerId].getObject3D().position)

            laserGenerators.forEach((laserGenerator) => {
                laserGenerator.lasers.forEach(laser => {
                    laser.testCollisions();
                });
            });

            //CONNECTORS RING
            connectors.concat(receivers).forEach(element => {
                element.ring.rotation.z += 0.04;
                if (element.object3D.userData.haveLaser) {
                    element.ring.material.color.setHex(0xff2323);
                } else {
                    element.ring.material.color.setHex(0x92ff92);
                }
            });
            connectors.concat(receivers).forEach(element => {
                if (element.object3D.userData.sendLaser && element.posX == players[playerId].posX && element.posZ == players[playerId].posZ) {
                    connectors.concat(receivers).forEach(element2 => {
                        element2.ring.material.color.setHex(0xff2323);
                    });
                }
            });

            //WIN WYGRANA
            var win = true;
            for (var i = 0; i < receiversWin.length; i++) {
                if (!receiversWin[i].object3D.userData.haveLaser) { win = false };
            }
            if (win && !winBoard) {
                //ENDGAME
                document.getElementById("root").style.opacity = "0";
                document.getElementById("end").style.opacity = "1";
                winBoard = true;
                setTimeout(function () {
                    document.getElementById("end").style.opacity = "0";
                    levelsShow();
                    // document.getElementById("waitingScreen").style.opacity = "1";
                    // loadLevel('doorTest');
                }, 2000)
            }
        }
        stats.end();
        requestAnimationFrame(render);
        renderer.render(scene, camera);
    }

    function wallGenerate(x, z, rotation) {
        var wall = wallTemplate.clone();
        wall.position.set(x, 15, z);
        wall.rotation.y = rotation;
        wall.material = wallTemplate.material;
        scene.add(wall);
        walls.push(wall);
    }

    function glassGenerate(x, z, rotation) {
        var glass = glassTemplate.clone();
        glass.position.set(x, 15, z);
        glass.rotation.y = rotation;
        glass.material = glassTemplate.material;
        glass.material.transparent = true;
        glass.material.opacity = 0.4;
        scene.add(glass);
        glasses.push(glass);
    }

    function fogGenerate(x, z, rotation) {
        var fog = fogTemplate.clone();
        fog.position.set(x, 15, z);
        fog.rotation.y = rotation;
        fog.material = fogTemplate.material;
        fog.material.transparent = true;
        fog.material.opacity = 0.4;
        scene.add(fog);
        fogs.push(fog);
    }
}