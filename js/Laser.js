class Laser {
    constructor(position, direction, generator, startConnector, first) {
        this.direction = direction;
        this.position = position;
        this.connector = null;
        this.startConnector = startConnector;
        this.lastIntersect = 0;
        this.generator = generator;
        this.first = first;

        this.createRaycaster();
    }

    createRaycaster() {
        this.raycaster = new THREE.Raycaster();
        this.intersects;
        this.testCollisions();
    }

    testCollisions() {
        if (this.first) {
            this.raycaster.set(this.position, this.direction.normalize());
        }
        else {
            this.raycaster.set(this.position, this.direction.clone().sub(this.position.clone()).normalize())
        }

        this.intersects = this.raycaster.intersectObjects(objects.concat(walls.concat(fogs)), true);
        if (this.intersects.length != 0) {
            if (this.lastIntersect.distance != this.intersects[0].distance) {
                this.findUserData(this.intersects[0].object);
                this.onRecivierCollision();
                this.lastIntersect = this.intersects[0];
            }
        }
    }

    createLaser() {
        var geometry = new THREE.BoxGeometry(this.intersects[0].distance, 1, 1);
        var material = new THREE.MeshBasicMaterial({ color: 0xff0000 });
        this.object3D = new THREE.Mesh(geometry, material);
        this.object3D.position.set((this.position.x + this.intersects[0].point.x) / 2, this.position.y, (this.position.z + this.intersects[0].point.z) / 2);
        var angle = Math.atan2(
            this.position.clone().x - this.intersects[0].point.x,
            this.position.clone().z - this.intersects[0].point.z
        );
        this.object3D.rotation.y = angle + Math.PI / 2;
        scene.add(this.object3D);
    }

    changeLaser() {
        var geometry = new THREE.BoxGeometry(this.intersects[0].distance, 1, 1);
        this.object3D.geometry = geometry;
        this.object3D.position.set((this.position.x + this.intersects[0].point.x) / 2, this.position.y, (this.position.z + this.intersects[0].point.z) / 2);
        var angle = Math.atan2(
            this.position.clone().x - this.intersects[0].point.x,
            this.position.clone().z - this.intersects[0].point.z
        );
        this.object3D.rotation.y = angle + Math.PI / 2;
    }

    findUserData(data) {
        if (Object.keys(data.userData).length != 0)
            this.colObject = data;
        else
            this.findUserData(data.parent);
    }

    onRecivierCollision() {
        if (this.colObject != null) {
            if (this.receiver != null) {
                this.receiver.userData.haveLaser = false;
                this.receiver = null;
                laserGenerators.forEach((laserGenerator) => {
                    var laserIndex = laserGenerator.lasers.indexOf(this);
                    if (laserIndex != -1) {
                        this.clearLasers(laserIndex, laserGenerator);
                    }
                });
            }
            if (this.connector != null) {
                this.connector.userData.haveLaser = false;
                this.connector.userData.sendLaser = false;
                this.connector = null;
                laserGenerators.forEach((laserGenerator) => {
                    var laserIndex = laserGenerator.lasers.indexOf(this);
                    if (laserIndex != -1) {
                        this.clearLasers(laserIndex, laserGenerator);
                    }
                });
            }
            switch (this.colObject.userData.name) {
                case "connector":
                    this.connector = this.colObject;
                    this.connector.userData.haveLaser = true;
                    this.connector.userData.laser = this;
                    if (this.object3D == null)
                        this.createLaser();
                    else
                        this.changeLaser();
                    break;
                case "receiver":
                    this.receiver = this.colObject;
                    this.receiver.userData.haveLaser = true;
                    if (this.object3D == null)
                        this.createLaser();
                    else
                        this.changeLaser();
                    break;
                default:
                    if (this.object3D == null) {
                        this.createLaser();
                    }
                    else {
                        laserGenerators.forEach((laserGenerator) => {
                            var laserIndex = laserGenerator.lasers.indexOf(this);
                            if (laserIndex != -1) {
                                this.clearLasers(laserIndex, laserGenerator);
                            }
                        });
                    }
                    break;
            }
        }
    }

    clearLasers(laserIndex, laserGenerator) {
        while (laserGenerator.lasers.length != laserIndex + 1 && laserGenerator.lasers.length > 1) {
            if (laserGenerator.lasers[laserGenerator.lasers.length - 1].connector != null) {
                laserGenerator.lasers[laserGenerator.lasers.length - 1].connector.userData.haveLaser = false;
            }
            if (laserGenerator.lasers[laserGenerator.lasers.length - 1].receiver != null) {
                laserGenerator.lasers[laserGenerator.lasers.length - 1].receiver.userData.haveLaser = false;
            }
            laserGenerator.lasers[laserGenerator.lasers.length - 1].startConnector.object3D.userData.sendLaser = false;
            scene.remove(laserGenerator.lasers.pop().object3D);
        }
        this.changeLaser();
    }
}
