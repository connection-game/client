const dirToVector = {
    "top": new THREE.Vector3(0, 0, -1),
    "topRight": new THREE.Vector3(1, 0, -1),
    "right": new THREE.Vector3(1, 0, 0),
    "rightDown": new THREE.Vector3(1, 0, 1),
    "down": new THREE.Vector3(0, 0, 1),
    "downLeft": new THREE.Vector3(-1, 0, 1),
    "left": new THREE.Vector3(-1, 0, 0),
    "leftTop": new THREE.Vector3(-1, 0, -1),
}

const dirToRotation = {
    "top": Math.PI,
    "topRight": Math.PI * (3 / 4),
    "right": Math.PI / 2,
    "rightDown": Math.PI / 4,
    "down": 0,
    "downLeft": Math.PI * (7 / 4),
    "left": Math.PI * 1.5,
    "leftTop": Math.PI * (5 / 4),
}