class Floor extends THREE.Mesh {
    constructor(material) {
        super();
        this.material = material;
        this.createFloor();
    }

    createFloor() {
        var geometry = new THREE.BoxGeometry(Settings.width, 1, Settings.width);
        this.geometry = geometry;
        this.userData.name = "floor";
    }
}