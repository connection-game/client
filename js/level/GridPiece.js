class GridPiece {
    constructor(x, z) {
        this.x = x;
        this.z = z;
        this.createMesh();
    }

    createMesh() {
        var geometry = new THREE.BoxBufferGeometry(Settings.width, 0.01, Settings.width, 0);
        var material = new THREE.MeshBasicMaterial({ color: 0x333333, wireframe: true });
        this.object3D = new THREE.Mesh(geometry, material);
        this.object3D.position.x = this.x * Settings.width + Settings.width / 2;
        this.object3D.position.z = this.z * Settings.width + Settings.width / 2;
    }
}