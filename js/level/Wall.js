class Wall extends THREE.Mesh {
    constructor(material) {
        super();
        this.material = material;
        this.createWall();
    }

    createWall() {
        var geometry = new THREE.BoxGeometry(Settings.width, 30, 3);
        this.geometry = geometry;
        this.userData.name = "wall";
    }
}