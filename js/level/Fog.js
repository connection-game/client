class Fog extends THREE.Mesh {
    constructor(material) {
        super();
        this.material = material;
        this.createFog();
    }

    createFog() {
        var geometry = new THREE.BoxGeometry(Settings.width, 30, 3);
        this.geometry = geometry;
        this.userData.name = "fog";
    }
}