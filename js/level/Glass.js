class Glass extends THREE.Mesh {
    constructor(material) {
        super();
        this.material = material;
        this.createGlass();
    }

    createGlass() {
        var geometry = new THREE.BoxGeometry(Settings.width, 30, 3);
        this.geometry = geometry;
        this.userData.name = "glass";
    }
}