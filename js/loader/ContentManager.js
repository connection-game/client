"use strict";
class ContentManager {
    constructor() {
        this.promises = [];
        this.data = {};
    }

    addFetch(name, url) {
        this.promises.push(new Promise((resolve, reject) => {
            fetch(url)
                .then(data => {
                    resolve(data);
                    this.data[name] = data;
                })
                .catch(reject);
        }));
    }

    addPhongMaterial(name, url) {
        this.promises.push(new Promise((resolve, reject) => {
            new THREE.TextureLoader().load(url, (texture) => {
                var material = new THREE.MeshPhongMaterial({
                    map: texture
                });
                this.data[name] = material;
                resolve(material);
            }, undefined, reject);
        }));
    }

    addModel(name, url) {
        this.promises.push(new Promise((resolve, reject) => {
            new THREE.GLTFLoader().load(url, (model) => {
                this.data[name] = model;
                resolve(model);
            }, undefined, reject);
        }));
    }

    loaded() {
        return Promise.all(this.promises);
    }
}